
/*
js:
    1. transformer une classe HTML en variable
        -injecter des classes
        -injecter du style
        -injecter du texte
    2. transformer une une variable en classe HTML
*/

const baliseH1=document.querySelector("h1");
const clickSuisse = document.querySelector(".click-event");
const btn1 = document.querySelector("#btn-1");
const btn2 = document.querySelector("#btn-2");
const response = document.querySelector("p");
const mouse = document.querySelector(".moove");
const keypressContainer = document.querySelector(".keypress");
const keyId = document.getElementById("key");
const scrollNav = document.querySelector("nav");
const inputName = document.querySelector('input[type="text"]');
const selectDev = document.querySelector("select");
const formDev = document.querySelector("form");
const formDiv = document.querySelector('form > div');
const boxes = document.querySelectorAll('.box')
const underLigne = document.querySelector('.trait')

const bubble = document.createElement("span")

//selector
baliseH1.style.background="yellow";

//click
clickSuisse.addEventListener('click',function () {
   clickSuisse.classList.toggle("style__inject-question");
})

btn1.addEventListener('click', function () {
    response.classList.toggle("style__inject-show-response");
    response.style.background = "green";
})

btn2.addEventListener('click', function () {
    response.classList.toggle("style__inject-show-response");
    response.style.background = "red";
})

//Mouse
window.addEventListener('mousemove', function (e) {
    mouse.style.left=e.pageX + "px";
    mouse.style.top=e.pageY + "px";
})

mouse.addEventListener('mousedown', function () {

    mouse.style.transform = "scale(2)   ";
    mouse.style.border = "2px solid teal   ";
})
mouse.addEventListener('mouseup', function () {
    mouse.style.transform = "scale(3)  ";
    mouse.style.border = "2px solid red   ";
})
clickSuisse.addEventListener('mouseenter', function () {
    btn2.style.background="rgba(0,0,0,0.6)";
})
clickSuisse.addEventListener('mouseout', function () {
    btn1.style.background="pink";
})
response.addEventListener('mouseover', function () {
    response.style.transform = "rotate(2deg)";
})

//KeyPress
document.addEventListener('keypress', function (e) {
    keyId.textContent=e.key;

    if (e.key=== "j") {
        keypressContainer.style.background = "blue";
    }else{
        keypressContainer.style.background = "green";
    }
})

//scroll
window.addEventListener('scroll', function () {
    if (window.scrollY > 10) {
        scrollNav.style.top = 0;
    }else{
        scrollNav.style.top = "-100px";
    }
})

//form input/submit/
let pseudo = "";
inputName.addEventListener('input', function (e) {
    pseudo = e.target.value;
})

let langage = "";
selectDev.addEventListener('input', function (e) {
  langage = e.target.value;
})

formDev.addEventListener('submit', function (e) {
    e.preventDefault(); /*no refresh*/
    if (cgv.checked) {
        formDiv.innerHTML = "<h3> Mon nom est " + pseudo  + "</h3>";
    } else {
        alert("veuillez accepter les CGV")
    }
})

//load
window.addEventListener('load', function () {
 //   console.log("chargement en cours ...");
})

// forEach
boxes.forEach((box) => {
    box.addEventListener('click', function (e) {
        e.target.style.transform = "scale(0.7)";
    })
})

//text injection
const buttons = document.querySelectorAll(".btn");
const result = document.getElementById("result");

buttons.forEach((btn) => {
    btn.addEventListener('click', function (e) {
        result.textContent += e.target.id;
    })
});

equal.addEventListener('click', function () {
    result.textContent = eval (result.textContent);
})
clear.addEventListener('click', function () {
    result.textContent = "";
})

//setProperty
window.addEventListener('click', function (e) {
    underLigne.style.setProperty("--x",  "200px");
    underLigne.style.setProperty("--y",  "100px");
})

//createElement
document.body.appendChild(bubble);
bubble.classList.add("boule");
bubble.addEventListener('mouseover', function () {
    bubble.style.background = "red";
})

